const url = "https://noroff-komputer-store-api.herokuapp.com";
let loanButton = document.getElementsByClassName("getloan");
let pBalance = document.getElementById("p-balance");
let pLoans = document.getElementById("p-loans");
let pPay = document.getElementById("p-pay");
let selectForm = document.getElementById("form-select");
let computerInfo = document.getElementById("computer-specs");
let computerTitle = document.getElementById("computer-title");
let computerT = document.getElementById("laptops");
let payAllBtn = document.getElementById("getPayAll");
let totalBalance = 5000;
let totalLoans = false;
let loans = 0;
let totalSalary = 0;
pPay.innerHTML = totalSalary;
pBalance.innerHTML = totalBalance;
pLoans.innerHTML = loans;

/*
  Get data from the given API 
*/
const getData = () => {
  fetch(`${url}/computers`)
    .then((res) => res.json())
    .then((data) => printComputers(data));
};

/*
  EventListener for clicks for every Button with the right ID.
*/
window.addEventListener("click", (e) => {
  if (e.target.id === "getloan") {
    getLoan();
  }
  if (e.target.id === "getBank") {
    sendMoneyToBank();
  }
  if (e.target.id === "getWork") {
    salaryForWork();
  }
  if (e.target.id === "getPayAll") {
    rePay();
  }
});

/*
  Looping out data from the API and create html elements for computers and images, and for selects and options.
  Event for buy-button checks if you can buy or not.
*/
const printComputers = (data) => {
  data.forEach((computer) => {
    let colDiv12 = document.createElement("div");
    colDiv12.setAttribute("class", "col-12 myCol");
    colDiv12.setAttribute("id", `${computer.id}`);
    colDiv12.style.display = "none";
    computerT.appendChild(colDiv12);
    let colDiv3 = document.createElement("div");
    colDiv3.setAttribute("class", "col-3 d-flex justify-content-center");
    colDiv3.setAttribute("id", "image");
    colDiv12.appendChild(colDiv3);
    let colDiv6 = document.createElement("div");
    colDiv6.setAttribute("class", "col-6");
    colDiv6.setAttribute("id", "text-computer");
    colDiv12.appendChild(colDiv6);
    let colDiv = document.createElement("div");
    colDiv.setAttribute("class", "col-3 d-flex stylecol");
    colDiv.setAttribute("id", "price-computer");
    colDiv12.appendChild(colDiv);
    let imageTag = document.createElement("img");
    imageTag.setAttribute("id", `${computer.id}`);
    imageTag.setAttribute("class", "computer-images");
    imageTag.setAttribute("src", `${url}/${computer.image}`);
    colDiv3.appendChild(imageTag);
    let laptopTitle = document.createElement("h2");
    laptopTitle.innerHTML = computer.title;
    colDiv6.appendChild(laptopTitle);
    let laptopText = document.createElement("p");
    laptopText.setAttribute("class", "laptoptext");
    laptopText.innerHTML = computer.specs;
    colDiv6.appendChild(laptopText);
    let priceTitle = document.createElement("h2");
    priceTitle.setAttribute("class", "myprice");
    priceTitle.innerHTML = `${computer.price}kr`;
    colDiv.appendChild(priceTitle);
    let buyBtn = document.createElement("button");
    buyBtn.setAttribute("id", `${computer.id}`);
    buyBtn.setAttribute("class", "buyBtn");
    buyBtn.innerHTML = "BUY NOW";
    colDiv.appendChild(buyBtn);

    buyBtn.addEventListener("click", function (e) {
      if (computer.id == e.target.id && totalBalance >= computer.price) {
        totalBalance = totalBalance - computer.price;
        pBalance.innerHTML = totalBalance;
        alert("Yay you just bought an amazing computer!");
      } else {
        alert("You dont have enough money!");
      }
    });

    let option = document.createElement("option");
    option.setAttribute("id", computer.id);
    let text = document.createTextNode(computer.title);
    option.appendChild(text);
    selectForm.appendChild(option);
    let id = selectForm.options[selectForm.selectedIndex].id;
    if (computer.id == id) {
      computerInfo.innerHTML = computer.description;
      computerTitle.innerHTML = computer.title;
      colDiv12.style.display = "flex";
    }
    if (computer.id == 5) {
      imageTag.setAttribute(
        "src",
        "https://noroff-komputer-store-api.herokuapp.com/assets/images/5.png"
      );
    }
    window.addEventListener("change", () => {
      colDiv12.style.display = "none";
      let id = selectForm.options[selectForm.selectedIndex].id;
      if (computer.id == id) {
        computerInfo.innerHTML = computer.description;
        computerTitle.innerHTML = computer.title;
        colDiv12.style.display = "flex";
      }
    });
  });
};

/*
  Method for getting a loan, checks if u have a loan and how much u are able to loan based on your current balance.
*/
const getLoan = () => {
  let loanValue = prompt("How much would you like to loan?");
  if (totalLoans === true || parseInt(loanValue) > totalBalance * 2) {
    alert("U were not able to take a loan!");
  } else {
    totalBalance += parseInt(loanValue);
    loans += parseInt(loanValue);
    pBalance.innerHTML = totalBalance;
    pLoans.innerHTML = parseInt(loanValue);
    totalLoans = true;
    payAllBtn.style.display = "flex";
  }
};

/*
  Method for sending money from ur work-balance to your bank. Checks if u have a loan and if u do, remove 10% from salary and take off 10% from loan. And if loan gets to 0 set totalLoans to false.
  else continue and set values to each element.
*/
const sendMoneyToBank = () => {
  if (totalLoans === true) {
    if (loans - totalSalary * 0.1 < 0) {
      totalBalance =
        totalBalance +
        (totalSalary * 0.1 - loans) +
        (totalSalary - totalSalary * 0.1);
      loans = 0;
      totalLoans = false;
      totalSalary = 0;
      pBalance.innerHTML = totalBalance;
      pLoans.innerHTML = loans;
      pPay.innerHTML = totalSalary;
    }
    loans -= totalSalary / 10;
    totalSalary -= totalSalary / 10;
    totalBalance += totalSalary;
    pBalance.innerHTML = totalBalance;
    totalSalary = 0;
    pPay.innerHTML = totalSalary;
    pLoans.innerHTML = loans;
    if (loans === 0) {
      totalLoans = false;
    }
  } else {
    totalBalance += totalSalary;
    pBalance.innerHTML = totalBalance;
    totalSalary = 0;
    pPay.innerHTML = totalSalary;
  }
};

/*
  Work button adds up 100kr each click.
*/
const salaryForWork = () => {
  let salary = 100;
  totalSalary += salary;
  pPay.innerHTML = totalSalary;
};

/*
  Repay button, checks if u have a loan and if u do remove the current loan with the salary. If the salary is higher than the loan, rest of the salary should be updated.
*/
const rePay = () => {
  if (totalLoans === true && loans <= totalSalary) {
    totalSalary = totalSalary - loans;
    loans = 0;
    pLoans.innerHTML = loans;
    pPay.innerHTML = totalSalary;
    totalLoans = false;
    payAllBtn.style.display = "none";
  } else if (totalLoans === true && loans > totalSalary) {
    loans = loans - totalSalary;
    pLoans.innerHTML = loans;
    totalSalary = 0;
    pPay.innerHTML = totalSalary;
  }
};

/*
  On window load, fetch all data from the API.
*/
window.onload = getData();
